package services;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;


import com.sabaq.sabaqlauncher.PasswordWindowActivity;

import java.util.List;


public class LockService extends Service {
    private int mStartMode;
    private IBinder mBinder;
    private boolean mAllowRebind;
    private Runnable onEveryNanoSecond;
    private Handler myHandler = new Handler();
    private ActivityManager mActivityManager;
    private String mLastPackageName = "";
    private String mCurrentPackageName = "";
    public static boolean lock = true;
    private String[] arrLockApps = {"com.android.settings", "one.sabaq.com.sabaqone", "com.android.packageinstaller"};


    public LockService() {

    }

    public void onCreate() {
        Log.d("Service Create", "CREATE");
        mActivityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
    }


    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service Start", "START");

        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

        onEveryNanoSecond = new Runnable() {

            @Override
            public void run() {
                mCurrentPackageName = getTopPackageName();
//                if (!mCurrentPackageName.equals(mLastPackageName)) {
                    Log.d("Service App Changed", "appchanged " + " (" +
                            mLastPackageName + ">"
                            + mCurrentPackageName + ")");

                    // prepare for next call
                    mLastPackageName = mCurrentPackageName;

//                        if (mCurrentPackageName.equals(arrLockApps[0])) {
                    Intent in = new Intent(LockService.this, PasswordWindowActivity.class);
                    in.putExtra("settings", mCurrentPackageName);
                    in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(in);
                    Log.d("Service Start", arrLockApps[0]);
                    Toast.makeText(LockService.this, "Setting", Toast.LENGTH_SHORT).show();
//                        }
//                        if (mCurrentPackageName.equals(arrLockApps[i])) {
//                        lock = true;
//                        }

//                }
                myHandler.postDelayed(onEveryNanoSecond, 100);
            }
        };
        myHandler.postDelayed(onEveryNanoSecond, 100);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1, restartServicePI);
    }

    public IBinder onBind(Intent intent) {
        Log.d("Service Bind", "BIND");
        return mBinder;
    }


    public boolean onUnbind(Intent intent) {
        Log.d("Service Unbind", "UNBIND");
        return mAllowRebind;
    }


    public void onRebind(Intent intent) {
        Log.d("Service Rebind", "REBIND");
    }


    public void onDestroy() {
        Log.d("Service Destroy", "DESTROY");
        Toast.makeText(this, "Service Destroy", Toast.LENGTH_SHORT).show();
        myHandler.removeCallbacks(onEveryNanoSecond);
    }

    private String getTopPackageName() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
////            return mActivityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
//            return getApplicationContext().getPackageName();
//
//        } else {
//            // Hack, see
//            // http://stackoverflow.com/questions/24625936/getrunningtasks-doesnt-work-in-android-l/27140347#27140347
//            final List<ActivityManager.RunningAppProcessInfo> pis = mActivityManager.getRunningAppProcesses();
//            for (ActivityManager.RunningAppProcessInfo pi : pis) {
//                if (pi.pkgList.length == 1) return pi.pkgList[0];
//            }
//
//            return getApplicationContext().getPackageName();
//        }
        String packageName;
        String className;
        ActivityManager amen = (ActivityManager) getBaseContext().getSystemService(getApplicationContext().ACTIVITY_SERVICE);
        packageName = amen.getRunningTasks(10).get(0).topActivity.getPackageName();
        className = amen.getRunningTasks(10).get(0).topActivity.getClassName();
        return packageName;
    }

    private void setSettingPanel() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

// display the current class name
        Toast.makeText(getApplicationContext(), taskInfo.get(0).topActivity.getClassName(), Toast.LENGTH_LONG).show();
        ComponentName componentInfo = taskInfo.get(0).topActivity;

// current class package name
        String packageName = componentInfo.getPackageName();
    }
}
