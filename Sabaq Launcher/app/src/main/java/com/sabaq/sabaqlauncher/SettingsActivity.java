package com.sabaq.sabaqlauncher;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import services.LockService;


public class SettingsActivity extends Activity {
    String msg = "Android : ";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Log.d(msg, "The onCreate() event");
        isStoragePermissionGranted();
    }

    public void startService(View view) {
        startService(new Intent(getBaseContext(), LockService.class));
    }

    // Method to stop the service
    public void stopService(View view) {
        stopService(new Intent(getBaseContext(), LockService.class));
    }

    // Method to stop the service
    public void disableAll(View view) {
        executeCommand("setprop persist.sys.usb.config none");
    }

    public void enableAll(View view) {
        executeCommand("setprop persist.sys.usb.config mtp,adb");
    }

    void executeCommand(String com) {
        ArrayList<String> commandLine = new ArrayList<String>();
        commandLine.add(com);//$NON-NLS-1$


        Process process = null;
        try {
            process = Runtime.getRuntime().exec(commandLine.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    private void setSettingPanel() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

// display the current class name
        Toast.makeText(getApplicationContext(), taskInfo.get(0).topActivity.getClassName(), Toast.LENGTH_LONG).show();
        ComponentName componentInfo = taskInfo.get(0).topActivity;

// current class package name
        String packageName = componentInfo.getPackageName();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.GET_TASKS)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("", "Permission is granted");
                return true;
            } else {

                Log.v("", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_TASKS}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }
}
