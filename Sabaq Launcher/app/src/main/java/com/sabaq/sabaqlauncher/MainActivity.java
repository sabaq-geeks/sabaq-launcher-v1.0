package com.sabaq.sabaqlauncher;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.sabaq.sabaqlauncher.Helper.BaseActivity;
import com.sabaq.sabaqlauncher.Helper.Helper;
import com.sabaq.sabaqlauncher.animation.MyBounceInterpolator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import services.LockService;

public class MainActivity extends BaseActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private GridViewAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
    private boolean isHidden = true, isClick = false;
    Context context;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void resetPreferredLauncherAndOpenChooser(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, MainActivity.class);
        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        Intent selector = new Intent(Intent.ACTION_MAIN);
        selector.addCategory(Intent.CATEGORY_HOME);
        selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(selector);

        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        }
        setContentView(R.layout.fragment_recycler_list_view);

        loadApps(isHidden);
        context = this;
        ImageButton btnHide = findViewById(R.id.btnHide);
//        softKeyBoardDownSystemUiHide(R.id.rl_container);
//        setSoftKeyBoardOffWhenActivityLaunch();
        btnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClick) {
                    isClick = true;
                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    v.startAnimation(myAnim);
                    waitTillAnimation();
                }
            }
        });
        onTouchScreen();
//        hidStatusBar(findViewById(R.id.rl_container));
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(0, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void loadApps(boolean isSabaqOnly) {
        PackageManager manager;
        List<AppDetail> apps;

        manager = getPackageManager();
        apps = new ArrayList<>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {
            AppDetail app = new AppDetail();
            Drawable d = null;
            try {
                Resources res = manager.getResourcesForApplication(ri.activityInfo.applicationInfo);
                int iconID = ri.getIconResource();
                if (iconID != 0) {
                    try {
                        d = res.getDrawableForDensity(iconID, DisplayMetrics.DENSITY_XHIGH, null);
                    } catch (Resources.NotFoundException e) {
                        d = null;
                    }

                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (isSabaqOnly && (ri.activityInfo.packageName.contains("com.meraSABAQ") ||
                    ri.activityInfo.packageName.contains("com.sabaq.muse") || ri.activityInfo.packageName.contains("com.sabaq.Muse") ||
                    ri.activityInfo.packageName.contains("com.sabaq.Muse1") || ri.activityInfo.packageName.contains("com.sabaq.Muse2") ||
                    ri.activityInfo.packageName.contains("com.sabaq.Muse3") || ri.activityInfo.packageName.contains("com.sabaq.Muse4") ||
                    ri.activityInfo.packageName.contains("com.sabaq.Muse5") || ri.activityInfo.packageName.contains("com.sabaq.MuseKg") ||
                    ri.activityInfo.packageName.contains("com.sabaq.cra") || ri.activityInfo.packageName.contains("com.sabaq.ilmfoundation") || ri.activityInfo.packageName.contains("com.sabaq.location") ||
                    ri.activityInfo.packageName.contains("pk.org.sef.instalappGKG12") || ri.activityInfo.packageName.contains("pk.org.sef.instalappG345") || ri.activityInfo.packageName.contains("pk.org.sef.teacherguide")
            )) {
                app.label = ri.loadLabel(manager);
                app.name = ri.activityInfo.packageName;
                if (d != null) app.icon = d;
                else
                    app.icon = ri.activityInfo.loadIcon(manager);
                apps.add(app);
            } else if (!isSabaqOnly) {
                app.label = ri.loadLabel(manager);
                app.name = ri.activityInfo.packageName;
                if (d != null) app.icon = d;
                else
                    app.icon = ri.activityInfo.loadIcon(manager);
                apps.add(app);
            }
        }
        Log.i("sabaq", "size" + apps.size());
        //Generating recycler view from here
        //
        //noinspection ConstantConditions
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mLayoutManager = new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false);

        // drag & drop manager
        mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
//		mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
//				(NinePatchDrawable) ContextCompat.getDrawable(this, R.drawable.material_shadow_z3));

        // Start dragging after long press
        mRecyclerViewDragDropManager.setInitiateOnLongPress(true);
        mRecyclerViewDragDropManager.setInitiateOnMove(false);
        mRecyclerViewDragDropManager.setLongPressTimeout(750);

        //adapter
        final GridViewAdapter myItemAdapter = new GridViewAdapter(new ExampleDataProvider(apps), this);
        mAdapter = myItemAdapter;

        mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(myItemAdapter);      // wrap for dragging

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);
        mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);
        mRecyclerViewDragDropManager.setItemMoveMode(RecyclerViewDragDropManager.ITEM_MOVE_MODE_DEFAULT);
        mAdapter.setItemMoveMode(RecyclerViewDragDropManager.ITEM_MOVE_MODE_DEFAULT);

    }

    private void passwordDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.alert_edittext_dialog, null);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        final ImageButton passwordVisibility = promptsView
                .findViewById(R.id.ib_visibility);

        passwordVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordVisibility.setBackgroundResource(R.drawable.ic_visibility_white_24dp);
            }
        });
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                matchingPassword(userInput.getText().toString());
                                isClick = false;
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                hidSystemUi();
                                isClick = false;
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void matchingPassword(String passwordEnter) {
        if (getString(R.string.password_admin).equals(passwordEnter)) {

            getPackageManager().clearPackagePreferredActivities(getPackageName());
            finish();
        } else {
            Toast.makeText(context, "Your password is wrong", Toast.LENGTH_SHORT).show();
        }

    }

    private void onTouchScreen() {

        (findViewById(R.id.rl_container)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Helper.hidNaviagtionUi(MainActivity.this);
                return false;
            }
        });
    }

    private void waitTillAnimation() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                new Thread() {
                    public void run() {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                passwordDialog();
                            }
                        });
                    }
                }.start();
            }
        };
        timer.start();
    }

    private void hidSystemUi() {


        /*Transparent status bar*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        View decorView = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        hidSystemUi();
    }
}
