package com.sabaq.sabaqlauncher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import services.LockService;


public class PasswordWindowActivity extends Activity {

    private EditText etPass;
    private Button btnPass;
    private String password = "1234";
    private String value="";

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_window);

        etPass = (EditText)findViewById(R.id.etPass);
        btnPass = (Button)findViewById(R.id.btnPass);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            value = extras.getString("settings");
        }
        btnPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPass.getText().toString().trim().equals(password)) {
                    finish();
                    LockService.lock = false;
                    if(value.length()>1){
                    Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage(value);
                    startActivity( LaunchIntent );}
                } else {
                    Toast.makeText(PasswordWindowActivity.this, "Invalid Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
