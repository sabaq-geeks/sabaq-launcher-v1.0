package com.sabaq.sabaqlauncher;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;

import java.util.ArrayList;
import java.util.List;

public class AppsListRecyclerActivity extends Activity {
	private RecyclerView mRecyclerView;
	private RecyclerView.LayoutManager mLayoutManager;
	private GridViewAdapter mAdapter;
	private RecyclerView.Adapter mWrappedAdapter;
	private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
	private boolean isHidden=true;
	Context context;

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public static void resetPreferredLauncherAndOpenChooser(Context context) {
		PackageManager packageManager = context.getPackageManager();
		ComponentName componentName = new ComponentName(context, com.sabaq.sabaqlauncher.AppsListRecyclerActivity.class);
		packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

		Intent selector = new Intent(Intent.ACTION_MAIN);
		selector.addCategory(Intent.CATEGORY_HOME);
		selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(selector);

		packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window w = getWindow(); // in Activity's onCreate() for instance
			w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		}
		setContentView(R.layout.fragment_recycler_list_view);

//		resetPreferredLauncherAndOpenChooser(this);
		loadApps(isHidden);
		context=this;
		ImageButton btnHide= findViewById(R.id.btnHide);
		btnHide.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				if(isHidden){
//					loadApps(isHidden);
//					isHidden=false;
//				}
//				else {
//					loadApps(isHidden);
//					isHidden=true;	}
				startActivity(new Intent(context,SettingsActivity.class));
			}
		});
//		btnHide.setVisibility(View.GONE);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private void loadApps( boolean isSabaqOnly){
		PackageManager manager;
		List<AppDetail> apps;

		manager = getPackageManager();
		apps = new ArrayList<AppDetail>();
		
		Intent i = new Intent(Intent.ACTION_MAIN, null);
		i.addCategory(Intent.CATEGORY_LAUNCHER);

		List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
		for(ResolveInfo ri:availableActivities){
			AppDetail app = new AppDetail();
			Drawable d=null;
			try {
				Resources res = manager.getResourcesForApplication(ri.activityInfo.applicationInfo);
				int iconID=ri.getIconResource();
				if (iconID != 0) {
				try {
					d = res.getDrawableForDensity(iconID, DisplayMetrics.DENSITY_XHIGH,null);
				} catch (Resources.NotFoundException e) {
					d = null;
				}

			}
			} catch (PackageManager.NameNotFoundException e) {
				e.printStackTrace();
			}

			if(isSabaqOnly && ri.activityInfo.packageName.contains("sabaq.com")){
			app.label = ri.loadLabel(manager);
			app.name = ri.activityInfo.packageName;
			if(d!=null)				app.icon = d;
			else
			app.icon = ri.activityInfo.loadIcon(manager);
			apps.add(app);
			}else if (!isSabaqOnly){
				app.label = ri.loadLabel(manager);
				app.name = ri.activityInfo.packageName;
				if(d!=null)				app.icon = d;
				else
					app.icon = ri.activityInfo.loadIcon(manager);
				apps.add(app);
			}
		}
	Log.i("sabaq","size"+apps.size());
		//Generating recycler view from here
		//
		//noinspection ConstantConditions
		mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

		mLayoutManager = new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false);

		// drag & drop manager
		mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
//		mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
//				(NinePatchDrawable) ContextCompat.getDrawable(this, R.drawable.material_shadow_z3));

		// Start dragging after long press
		mRecyclerViewDragDropManager.setInitiateOnLongPress(true);
		mRecyclerViewDragDropManager.setInitiateOnMove(false);
		mRecyclerViewDragDropManager.setLongPressTimeout(750);

		//adapter
		final GridViewAdapter myItemAdapter = new GridViewAdapter(new ExampleDataProvider(apps),this);
		mAdapter = myItemAdapter;

		mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(myItemAdapter);      // wrap for dragging

		final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

		mRecyclerView.setLayoutManager(mLayoutManager);
		mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
		mRecyclerView.setItemAnimator(animator);
		mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);
		mRecyclerViewDragDropManager.setItemMoveMode(RecyclerViewDragDropManager.ITEM_MOVE_MODE_DEFAULT);
		mAdapter.setItemMoveMode(RecyclerViewDragDropManager.ITEM_MOVE_MODE_DEFAULT);

	}


}
